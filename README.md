# To run the project

Build the project:
```cmd
    gradle clean build
```

Build the docker image:
```cmd 
    docker build -t acme-apis .
```

Build the docker compose:
```cmd
    docker-compose build
```

Up and running the application through docker compose:
```cmd
    docker-compose up
```

Shot down the application:
```cmd
    Ctrl + C 
```
And
```cmd
    docker-compose down
```