package com.acme.api.order.entity;

import com.acme.api.order.enums.OrderStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Document(value = "order")
public class Order implements Serializable {

    private static final long serialVersionUID = 5598437541428028452L;

    @Id
    @NotNull
    @NotBlank
    private String id;

    @NotNull
    @NotBlank
    private String address;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate confirmationDate;

    @NotNull
    private OrderStatusEnum status;

    @NotNull
    @NotEmpty
    private List<Item> items;

    public Order() {
        //Default empty constructor for Jackson.
    }

    public Order(@NotNull @NotBlank String id, @NotNull @NotBlank String address,
                 @NotNull LocalDate confirmationDate, @NotNull OrderStatusEnum status,
                 @NotNull @NotEmpty List<Item> items) {
        this.id = id;
        this.address = address;
        this.confirmationDate = confirmationDate;
        this.status = status;
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public @NotNull LocalDate getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(@NotNull LocalDate confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public OrderStatusEnum getStatus() {
        return status;
    }

    public void setStatus(OrderStatusEnum status) {
        this.status = status;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Order order = (Order) o;
        return getId().equals(order.getId()) &&
                getAddress().equals(order.getAddress()) &&
                getConfirmationDate().equals(order.getConfirmationDate()) &&
                getStatus() == order.getStatus() &&
                getItems().equals(order.getItems());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAddress(), getConfirmationDate(), getStatus(), getItems());
    }
}
