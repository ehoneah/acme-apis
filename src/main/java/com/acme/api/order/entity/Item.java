package com.acme.api.order.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Document(value = "item")
public class Item implements Serializable {

    private static final long serialVersionUID = -2069365828324852208L;

    @Id
    @NotNull
    @NotBlank
    private String id;

    @NotNull
    @NotBlank
    private String description;

    @NotNull
    @PositiveOrZero
    private BigDecimal unityPrice;

    @PositiveOrZero
    private int quantity;

    public Item() {
        //Default empty constructor for Jackson.
    }

    public Item(@NotNull @NotBlank String id, @NotNull @NotBlank String description,
                @NotNull @NotBlank BigDecimal unityPrice, @NotNull @NotBlank int quantity) {
        this.id = id;
        this.description = description;
        this.unityPrice = unityPrice;
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getUnityPrice() {
        return unityPrice;
    }

    public void setUnityPrice(BigDecimal unityPrice) {
        this.unityPrice = unityPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Item item = (Item) o;
        return getQuantity() == item.getQuantity() &&
                getId().equals(item.getId()) &&
                getDescription().equals(item.getDescription()) &&
                getUnityPrice().equals(item.getUnityPrice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDescription(), getUnityPrice(), getQuantity());
    }
}
