package com.acme.api.order.enums;

public enum OrderStatusEnum {

    RECEIVED("Order Received"),
    CANCELED("Order Canceled"),
    REFUSED("Order Refused"),
    CONFIRMED("Order Confirmed"),
    REFUNDED("Order Refunded"),
    COMPLETED("Order Completed");

    private String description;

    OrderStatusEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
