package com.acme.api.order.repository;

import com.acme.api.order.entity.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface OrderRepository extends MongoRepository<Order, String > {

    /**
     * To use void and @NotNull annotation.
     * @param order Store object
     */
    default void saveOrder(@NotNull Order order){
        save(order);
    }

    @Override
    Optional<Order> findById(@NotNull @NotBlank String orderId);

}
