package com.acme.api.order;

import com.acme.api.exception.DomainBusinessException;
import com.acme.api.order.dto.OrderDTO;
import com.acme.api.order.entity.Order;
import com.acme.api.order.service.OrderService;
import com.acme.api.util.GsonUtil;
import com.google.gson.JsonObject;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@RestController
@RequestMapping(value = "/order")
@ApiResponses({
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 415, message = "Unsupported Media Type"),
        @ApiResponse(code = 422, message = "Unprocessable Entity")
})
public class OrderController {

    private final OrderService orderService;
    private final GsonUtil gsonUtil;

    public OrderController(OrderService orderService, GsonUtil gsonUtil) {
        this.orderService = orderService;
        this.gsonUtil = gsonUtil;
    }

    @PostMapping(
            value = "/v1/new",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(
            value = "Create new Order.",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            response = String.class
    )
    public ResponseEntity<String> addNewOrder(
            @RequestHeader(name = "Content-type", defaultValue = "application/json;charset=UTF-8")
            @NotNull final String contentType,
            @RequestBody @NotNull @Valid OrderDTO orderDTO) {

        orderService.createNewOrder(orderDTO);

        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "Successfully created the new Order.");

        return ResponseEntity.ok(gsonUtil.getSimpleStringJsonObject(jsonObject));
    }


    @GetMapping(
            value = "/v1/list",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(
            value = "Retrieve all Orders from the database.",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            response = String.class
    )
    public ResponseEntity<String> getAllOrders(
            @RequestHeader(name = "Content-type", defaultValue = "application/json;charset=UTF-8")
            @NotNull final String contentType) {

        final Optional<Iterable<Order>> optionalOrders = orderService.getAllStore();

        if (optionalOrders.isPresent()) {

            final String prettyStringJsonObject = gsonUtil.getSimpleStringJsonObject(optionalOrders.get());

            return ResponseEntity.ok(prettyStringJsonObject);

        } else {
            throw new DomainBusinessException();
        }
    }


    @GetMapping(
            value = "/v1/filter/{id}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(
            value = "Retrieve Order information by ID.",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            response = String.class
    )
    public ResponseEntity<String> getOrderById(
            @RequestHeader(name = "Content-type", defaultValue = "application/json;charset=UTF-8") @NotNull final String contentType,
            @PathVariable("id") @ApiParam("The ID from Order you want to search") @NotNull final String orderId) {

        final Optional<Order> optionalOrder = orderService.getStoreById(orderId);

        if (optionalOrder.isPresent()) {

            final String prettyStringJsonObject = gsonUtil.getSimpleStringJsonObject(optionalOrder.get());

            return ResponseEntity.ok(prettyStringJsonObject);

        } else {
            throw new DomainBusinessException();
        }
    }

    @PutMapping(
            value = "/v1/refund/{id}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(
            value = "Refund the Order by ID.",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            response = String.class
    )
    public ResponseEntity<String> refundOrder(
            @RequestHeader(name = "Content-type", defaultValue = "application/json;charset=UTF-8") @NotNull final String contentType,
            @PathVariable("id") @ApiParam("The ID from Order you want to refund")
            @NotNull @NotEmpty final String orderId) {

        orderService.refundOrder(orderId);

        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "Successfully refund the selected Order.");

        return ResponseEntity.ok(gsonUtil.getSimpleStringJsonObject(jsonObject));
    }
}
