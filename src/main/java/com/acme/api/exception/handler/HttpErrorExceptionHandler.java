package com.acme.api.exception.handler;

import com.acme.api.exception.DataBaseException;
import com.acme.api.exception.DomainBusinessException;
import com.acme.api.exception.InternalServerException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class HttpErrorExceptionHandler extends ResponseEntityExceptionHandler {

    public HttpErrorExceptionHandler() {
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(DomainBusinessException.class)
    @ResponseBody
    public ApiError domainBusinessException(DomainBusinessException exception) {
        return ApiError.fromHttpError(HttpStatus.UNPROCESSABLE_ENTITY, exception);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(InternalServerException.class)
    @ResponseBody
    public ApiError internalServerException(InternalServerException exception) {
        return ApiError.fromHttpError(HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(DataBaseException.class)
    @ResponseBody
    public ApiError dataBaseException(DataBaseException exception) {
        return ApiError.fromHttpError(HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }
}
