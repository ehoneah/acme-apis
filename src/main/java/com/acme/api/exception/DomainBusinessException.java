package com.acme.api.exception;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class DomainBusinessException extends RuntimeException {

    private static final long serialVersionUID = 4048417505695013037L;
    private static final String DEFAULT_ERROR_MESSAGE = "Sorry! Nothing was found. Please try again later.";

    public DomainBusinessException(@NotNull @NotBlank final String message) {
        super(message);
    }

    public DomainBusinessException() {
        super(DEFAULT_ERROR_MESSAGE);
    }
}
