package com.acme.api.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class GsonUtil {

    private Gson gson;

    public GsonUtil(Gson gson) {
        this.gson = gson;
    }

    public String getSimpleStringJsonObject(@NonNull final Object object) {
        return gson.toJson(object);
    }

    public String getPrettyStringJsonObject(@NonNull final Object object) {
        gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(object);
    }

}
