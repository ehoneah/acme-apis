package com.acme.api.store;

import com.acme.api.exception.DomainBusinessException;
import com.acme.api.store.dto.StoreDTO;
import com.acme.api.store.entity.Store;
import com.acme.api.store.service.StoreService;
import com.acme.api.util.GsonUtil;
import com.google.gson.JsonObject;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@RestController
@RequestMapping(value = "/store")
@ApiResponses({
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 415, message = "Unsupported Media Type"),
        @ApiResponse(code = 422, message = "Unprocessable Entity")
})
public class StoreController {

    private final StoreService storeService;
    private final GsonUtil gsonUtil;

    public StoreController(StoreService storeService, GsonUtil gsonUtil) {
        this.storeService = storeService;
        this.gsonUtil = gsonUtil;
    }

    @PostMapping(
            value = "/v1/new",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(
            value = "Create new Store.",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            response = String.class
    )
    public ResponseEntity<String> addNewStore(
            @RequestHeader(name = "Content-type", defaultValue = "application/json;charset=UTF-8")
            @NotNull final String contentType,
            @RequestBody @NotNull @Valid final StoreDTO storeDTO) {

        storeService.createNewStore(storeDTO);

        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "Successfully created the new Store.");

        return ResponseEntity.ok(gsonUtil.getSimpleStringJsonObject(jsonObject));
    }


    @GetMapping(
            value = "/v1/list",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(
            value = "Retrieve all Stores from the database.",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            response = String.class
    )
    public ResponseEntity<String> getAllStores(
            @RequestHeader(name = "Content-type", defaultValue = "application/json;charset=UTF-8")
            @NotNull final String contentType) {

        final Optional<Iterable<Store>> optionalStores = storeService.getAllStore();

        if (optionalStores.isPresent()) {

            final String prettyStringJsonObject = gsonUtil.getSimpleStringJsonObject(optionalStores.get());

            return ResponseEntity.ok(prettyStringJsonObject);

        } else {
            throw new DomainBusinessException();
        }
    }


    @GetMapping(
            value = "/v1/filter/{id}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(
            value = "Retrieve Store information by ID.",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            response = String.class
    )
    public ResponseEntity<String> getStoreById(
            @RequestHeader(name = "Content-type", defaultValue = "application/json;charset=UTF-8") @NotNull final String contentType,
            @PathVariable("id") @ApiParam("The ID from Store you want to search") @NotNull final String storeId) {

        final Optional<Store> storeOptional = storeService.getStoreById(storeId);

        if (storeOptional.isPresent()) {

            final String prettyStringJsonObject = gsonUtil.getSimpleStringJsonObject(storeOptional.get());

            return ResponseEntity.ok(prettyStringJsonObject);

        } else {
            throw new DomainBusinessException();
        }
    }


    @PutMapping(
            value = "/v1/update/{id}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(
            value = "Update Store information by ID.",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            response = String.class
    )
    public ResponseEntity<String> updateStore(
            @RequestHeader(name = "Content-type", defaultValue = "application/json;charset=UTF-8") @NonNull final String contentType,
            @PathVariable("id") @ApiParam("The ID from Store you want to update") @NonNull @NotEmpty final String storeId,
            @RequestBody @NotNull @Valid final StoreDTO storeDTO) {

        storeService.updateStoreInformation(storeId, storeDTO);

        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "Successfully update the selected Store.");

        return ResponseEntity.ok(gsonUtil.getSimpleStringJsonObject(jsonObject));
    }
}
