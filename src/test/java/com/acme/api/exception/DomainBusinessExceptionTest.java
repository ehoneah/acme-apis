package com.acme.api.exception;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DomainBusinessExceptionTest {

    private static final String DEFAULT_MESSAGE = "Something wrong is not right.";
    private DomainBusinessException domainBusinessException;

    @BeforeEach
    void setUp() {
        domainBusinessException = new DomainBusinessException(DEFAULT_MESSAGE);
    }

    @Test
    void someTests(){
        assertNotNull(domainBusinessException);

        assertEquals(DEFAULT_MESSAGE, domainBusinessException.getMessage());
    }
}