package com.acme.api.exception;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class InternalServerExceptionTest {

    private static final String DEFAULT_MESSAGE = "Something wrong is not right.";
    private InternalServerException internalServerException;

    @BeforeEach
    void setUp() {
        internalServerException = new InternalServerException(DEFAULT_MESSAGE);
    }

    @Test
    void someTests(){
        assertNotNull(internalServerException);

        assertEquals(DEFAULT_MESSAGE, internalServerException.getMessage());
    }

}