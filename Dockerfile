FROM openjdk:11-jre-slim

RUN mkdir -p /opt/app

ENV PROJECT_HOME /opt/app

COPY build/libs/*.jar $PROJECT_HOME/acme-apis.jar

WORKDIR $PROJECT_HOME

# Java Opts and the URI which will be used for connection to MongoDB.
CMD ["java", "-Dspring.data.mongodb.uri=mongodb://acme-apis-mongo:27017/acme-apis","-Djava.security.egd=file:/dev/./urandom","-jar","./acme-apis.jar"]
